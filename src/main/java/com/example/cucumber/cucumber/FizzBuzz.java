package com.example.cucumber.cucumber;

public class FizzBuzz {
	
	public FizzBuzz() {
		System.out.println(" --- inside a FizzBuzz constructor --- ");
	}

	public String play(int number) {
		if (number % 3 == 0 && number % 5 == 0 )
			return "FizzBuzz";
		else if (number % 3 == 0)
			return "Fizz";
		else if (number % 5 == 0)
			return "Buzz";
		else
			return "";
	}
}
