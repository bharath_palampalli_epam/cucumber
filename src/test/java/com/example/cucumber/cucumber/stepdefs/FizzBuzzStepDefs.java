package com.example.cucumber.cucumber.stepdefs;

import org.assertj.core.api.Assertions;

import com.example.cucumber.cucumber.FizzBuzz;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class FizzBuzzStepDefs {

	private FizzBuzz fizzBuzz;
	
	private String result;
	
	/** Below commented lines/steps are also valid */
	
//	@Given("^Create a FizzBuzz game play$")
//	public void createAFizzBuzzGamePlay() {
//		fizzBuzz = new FizzBuzz();
//	}
//	
//	@When("^I play with number (\\d+)$")
//	public void iPlayWithNumber(int number) {
//		result = fizzBuzz.play(number);
//	}
	
	@Given("Create a FizzBuzz game play")
	public void createAFizzBuzzGamePlays() {
		fizzBuzz = new FizzBuzz();
	}
	
	@Given("print something to the screen")
	public void print_something_to_the_screen() {
		System.out.println("printing something to the screen");
	}

	@When("I play with number {int}")
	public void i_play_with_number(Integer number) {
		result = fizzBuzz.play(number);
	}

	@Then("The result is {string}")
	public void the_result_is(String expectedResult) {
	    Assertions.assertThat(result).isEqualTo(expectedResult);
	}
}
