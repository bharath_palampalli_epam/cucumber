package com.example.cucumber.cucumber;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/")
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class CucumberApplicationTests {

	@Test
	public void contextLoads() {
	}

}
