#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@fizzBuzz
Feature: FizzBuzz game
			I want to play a game

	Background: 
		Given Create a FizzBuzz game play
		
  @tag1
  Scenario: Playing game to get Fizz
    #Given Create a FizzBuzz game play
    And print something to the screen
    When I play with number 3
    Then The result is "Fizz"

  @tag2
  Scenario Outline: Playing game to get Fizz or Buzz or both
    #Given Create a FizzBuzz game play
    When I play with number <number>
    Then The result is "<result>"

    Examples: 
      | number |  result  |
      |   5    |   Buzz   |
      |   15   | FizzBuzz |

